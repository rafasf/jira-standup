# Presentation Stand-up

Run stand-up as a presentation, each slides is a story and we discuss what is
necessary to get that value delivered.


## Usage

Create a configuration file with the desired values:

```edn
{
 :base-url "https://jira-server.com"
 :auth "Base Auth Token Here"
 :jql "project = 'Your Project' AND Sprint in openSprints() AND issuetype = Story"
 :fields "summary,assignee,status,customfield_10101,subtasks"
 :epic-key-field :customfield_10101
 :epic-jql "project = 'Your Project' AND issuetype = 'Epic'"
 :epic-name-field :customfield_10103
 :status-order {"In Testing" 0 "Ready for Testing" 1 "In Development" 2}
}
```

* `epic-key-field` is a custom field, please inform what is the name of the
  field in your configuration
* `epic-name-field` is a custom field, please inform what is the name of the
  field in your configuration

After downloading the `jar` for the app, run the following:

`$ STANDUP_CONFIG=/Absolute/Path/to/config.edn java -jar jira-standup.jar`

The application should be available in the port [3000](http://localhost:3000).

## License

See [LICENSE](./LICENSE)
