(ns jira-standup.client-test
  (:require [clojure.test :refer :all]
            [jira-standup.client :refer :all]))

(def jira-response
  {:body "{ \"issues\": [ { \"fields\": { \"assignee\": { \"displayName\": \"assignee\" }, \"status\": { \"name\": \"the status\" }, \"summary\": \"Story summary\" } } ] }"})

(testing "Search"
  (testing "return all issues from Jira's search result"
    (is (= [{:fields {:assignee {:displayName "assignee"}
                      :status {:name "the status"}
                      :summary "Story summary"}}]
           (issues-in jira-response)))))
