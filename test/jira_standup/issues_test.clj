(ns jira-standup.issues-test
  (:require [clojure.test :refer :all]
            [jira-standup.issues :refer :all]))

(def a-subtask
  {:key "SPROJ-123-1"
   :fields {:status {:name "In Progress"}
            :summary "Sub-task summary"}})

(def an-issue
  {:key "PROJ-123"
   :fields {:assignee {:displayName "Bob Jane"}
            :status {:name "In Development"}
            :subtasks [a-subtask]
            :customfield_epic "EPIC-123"
            :summary "Story summary here"}})

(testing "Issue"
  (testing "mapping from issue to story"
    (let [story-with-epic (partial to-story-using :customfield_epic)
          story (story-with-epic an-issue)]
      (is (= {:number "PROJ-123"
              :summary "Story summary here"
              :status "In Development"
              :assignee "Bob Jane"
              :epic {:number "EPIC-123"}
              :subtasks [{:number "SPROJ-123-1"
                          :summary "Sub-task summary"
                          :status "In Progress"}]}
             story))))

  (testing "mapping to story without assignee"
    (let [no-assignee-issue (update-in an-issue [:fields] assoc :assignee nil)
          story (to-story-using :epic-key-field no-assignee-issue)]
      (is (= "nobody"
             (story :assignee)))))

  (testing "mapping to epic"
    (let [epic-issue {:key "EPIC-123" :fields {:epic-name-field "the epic name"}}
          epic (to-epic-using :epic-name-field epic-issue)]
      (is (= {:number "EPIC-123"
              :name "the epic name"}
             epic)))))

(testing "Story with Epic"
  (testing "adding epic name"
    (let [epics [{:number "E12" :name "Cool E"}]
          story {:epic {:number "E12"}}]
      (is (= {:epic {:number "E12" :name "Cool E"}}
             (to-story-with-epic epics story)))))

  (testing "sorting by epic then status"
    (let [status-order {"In Testing" 0 "Ready for Testing" 1 "In Development" 2}
          stories [{:status "In Development" :epic {:number "A1"}}
                   {:status "In Testing" :epic {:number "A1"}}
                   {:status "Ready for Testing" :epic {:number "A1"}}
                   {:status "In Testing" :epic {:number "A0"}}]]
      (is (= [{:status "In Testing" :epic {:number "A0"}}
              {:status "In Testing" :epic {:number "A1"}}
              {:status "Ready for Testing" :epic {:number "A1"}}
              {:status "In Development" :epic {:number "A1"}}]
             (sorted stories status-order))))))
