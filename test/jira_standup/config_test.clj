(ns jira-standup.config-test
  (:require [clojure.test :refer :all]
            [clojure.java.io :as io]
            [jira-standup.config :refer :all]))

(testing "Read"
  (testing "returns the config"
    (is (= {:auth "blah"
            :jql "blahblah"
            :fields "yblahblah"}
           (read-config (io/resource "test_config.edn"))))))
