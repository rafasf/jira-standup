(defproject jira-standup "0.1.0"
  :description "Clean story list"
  :url "https://gitlab.com/rafasf/jira-standup"
  :license {:name "MIT License"
            :url "https://opensource.org/licenses/MIT"}
  :plugins [[lein-ring "0.12.5"]
            [lein-cljfmt "0.6.4"]]

  :dependencies [[org.clojure/clojure "1.10.0"]
                 [ring/ring-core "1.7.1"]
                 [ring/ring-jetty-adapter "1.7.1"]
                 [clj-http "3.9.1"]
                 [cheshire "5.8.1"]
                 [hiccup "1.0.5"]
                 [cljfmt "0.6.4"]]

  :ring {:handler jira-standup.core/handler}
  :main jira-standup.core
  :aot :all

  :resource-paths ["resources"]

  :profiles
  {:dev {:plugins [[com.jakemccrary/lein-test-refresh "0.24.1"]]}
   :test {:plugins [[lein-test-report-junit-xml "0.2.0"]]
          :dependencies [[pjstadig/humane-test-output "0.9.0"]]
          :injections [(require 'pjstadig.humane-test-output)
                       (pjstadig.humane-test-output/activate!)]}}

  :repl-options {:init-ns jira-standup.core})
