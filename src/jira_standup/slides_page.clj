(ns jira-standup.slides-page
  (:require [hiccup.core :refer :all]
            [clojure.string :refer [join lower-case split]]))

(defn class-of [text]
  (-> text (split #" ") join lower-case))

(defn to-subtask [{:keys [summary number status]}]
  [:div (str summary "(" number ")")
   [:span {:class (str "pill " (class-of status))} status]])

(defn subtasks-in [tasks]
  (map to-subtask tasks))

(defn story [base-url story]
  [:article {:class "slide"}
   [:p (get-in story [:epic :name] "No Epic")]
   [:h1 (story :summary)]
   [:h4 [:a {:href (str base-url "/browse/" (story :number)), :target "_blank"}
         [:strong (story :number)]]
    (str ", " (story :assignee) ", ")
    [:span {:class (str "pill " (class-of (story :status)))} (story :status)]]
   (subtasks-in (story :subtasks))])

(defn given [stories base-url]
  [:section {:class "slides"}
   (map (partial story base-url) stories)])
