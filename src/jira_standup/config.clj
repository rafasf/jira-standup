(ns jira-standup.config
  (:require [clojure.edn :as edn]))

(defn read-config [path]
  (->> path
       slurp
       edn/read-string))
