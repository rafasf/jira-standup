(ns jira-standup.core
  (:require [clojure.java.io :as io]
            [ring.adapter.jetty :as jetty]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [ring.middleware.not-modified :refer [wrap-not-modified]]
            [cheshire.core :refer [parse-string]]
            [jira-standup.issues :refer :all]
            [jira-standup.client :as jira]
            [jira-standup.slides-page :as slides]
            [jira-standup.config :as config]
            [hiccup.page :refer :all])
  (:gen-class))

(def config (config/read-config
             (or (System/getenv "STANDUP_CONFIG")
                 (io/resource "jira_standup.edn"))))

(def to-story
  (partial to-story-using (config :epic-key-field)))

(def to-epic
  (partial to-epic-using (config :epic-name-field)))

(defn to-detailed-stories [stories epics]
  (sorted (map #(to-story-with-epic epics %) stories)
          (config :status-order)))

(defn all-epics []
  (map to-epic (jira/search (config :base-url)
                            (config :auth)
                            (config :epic-jql)
                            (name (config :epic-name-field)))))

(defn all-stories []
  (map to-story (jira/search (config :base-url)
                             (config :auth)
                             (config :jql)
                             (config :fields))))

(defn handler [request]
  {:status 200
   :headers {"Content-Type" "text/html"}
   :body (html5
          [:head
           [:title "Stand-up"]
           (include-css "screen.css")]
          (slides/given (to-detailed-stories
                         (all-stories)
                         (all-epics))
                        (config :base-url)))})

(defn -main []
  (jetty/run-jetty
   (-> handler
       (wrap-resource "public")
       (wrap-content-type)
       (wrap-not-modified))
   {:port 3000}))
