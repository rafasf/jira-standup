(ns jira-standup.issues)

(defn to-subtask [subtask]
  (let [{number :key, {:keys [summary
                              status]} :fields} subtask]
    {:summary summary
     :status (status :name)
     :number number}))

(defn to-story-using [epic-key-field issue]
  (let [{number :key, {epic-number epic-key-field
                       :keys [summary
                              assignee
                              status
                              subtasks]} :fields} issue]
    {:summary summary
     :assignee (get assignee :displayName "nobody")
     :status (status :name)
     :epic {:number epic-number}
     :number number
     :subtasks (map to-subtask subtasks)}))

(defn to-story-with-epic [epics story]
  (let [epic (first (filter #(= (get-in story [:epic :number]) (get % :number)) epics))]
    (assoc story :epic epic)))

(defn to-epic-using [field-name epic-issue]
  {:number (epic-issue :key)
   :name (get-in epic-issue [:fields field-name])})

(defn sorted [stories status-order]
  (sort-by (juxt #(get-in % [:epic :number])
                 #(status-order (get % :status))) stories))

