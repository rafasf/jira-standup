(ns jira-standup.client
  (:require [clj-http.client :as client]
            [cheshire.core :refer [parse-string]]))

(defn str-to-map [text]
  (parse-string text true))

(defn issues-in [response]
  (-> response :body str-to-map :issues))

(defn search [base-url auth jql fields-to-return]
  (-> (client/get (str base-url "/rest/api/latest/search")
                  {:headers {"Authorization" (str "Basic " auth)}
                   :query-params {"jql" jql
                                  "fields" fields-to-return}
                   :accept :json})
      issues-in))
